FROM debian:12

COPY weatherboard /usr/bin/weatherboard
COPY weatherboard.service /etc/systemd/system/weatherboard.service

RUN apt-get -yq update && apt-get -yq install wkhtmltopdf

VOLUME /etc/weatherboard
WORKDIR /etc/weatherboard

ENV TZ=""

ENTRYPOINT ["/usr/bin/weatherboard"]
