# <img src="doc/icon.svg" width="36" height="36"> WeatherBoard

Weather dashboard for Kindle devices that reads data from Home Assistant (2024.8+)

Jailbreak is _not_ required.

![screenshot](doc/screenshot.png)

# Running

## Standalone

[Download executable](https://gitlab.com/klikini/weatherboard/-/jobs/artifacts/main/raw/weatherboard?job=build)

Run `./weatherboard` on the server.
A file named `weatherboard_config.toml` will be created in the current directory.
Fill in the missing data and then run it again.

## Docker

```sh
docker run registry.gitlab.com/klikini/weatherboard:latest \
   -v ./weatherboard:/etc/weatherboard \
   -p 8333:8333 \
   -e TZ=America/Los_Angeles
```

## Configuration

```toml
[HomeAssistant]
URL = "http://{HA IP}:8123"
Token = "{Get from user settings -> Long-Lived Access Tokens in HA}"

[Weather]
EntityID = "weather.{city}"
OtherStates = ["sensor.indoor_temperature", "sensor.indoor_humidity"] # optional
OtherStatesColumns = 1 # optional

[Web]
# Optional; default is shown.
# Add an address to use a specific interface,
# 0.0.0.0 to listen on all interfaces,
# or specify only the port to use the default interface.
ListenAddress = ":8333"

# Optional; default is shown.
# Set to 0 to disable auto-refresh.
RefreshSeconds = 900

# This section is optional unless you want to generate the dashboard as a PNG image by navigating to /image.
# See "Generating images instead" in the README for more about that.
[Image]
WkHtmlToImage = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltoimage.exe"
Width = 1024
Height = 758
```

## As a service

Create the file `/etc/systemd/system/weatherboard.service` with the following contents:

```conf
[Unit]
Description=WeatherBoard
After=network.target

[Service]
ExecStart=/srv/weatherboard/weatherboard
Restart=on-failure
WorkingDirectory=/srv/weatherboard

[Install]
WantedBy=multi-user.target
```

The file paths may need to be adjusted depending on where you put the executable.

Run `systemctl daemon-reload` to find the service file, then `systemctl start weatherboard` to start the server.
To automatically start the server on boot, run `systemctl enable weatherboard`.

# Using

1. Search for `~ds` on your Kindle (type it into the search box and press enter) to disable the screensaver.
   Don't worry; it will automatically re-enable on the next reboot.
2. Open your Kindle's browser (or, even better, [SkipStone](#skipstone))
   to `http://{server IP}:8333`.

The page will automatically refresh every 15 minutes, and the server will request new data
from Home Assistant every time the page loads. This will _not_ increase your weather API usage.

## Battery life

Not great. I got about 10 hours out of it using SkipStone, so you're probably going to want to leave it plugged in. I'm
not sure about the built-in browser.

## Built-in browser

By default, the "experimental browser" included with Kindle touch devices doesn't support landscape mode as it did on
older devices.
However, you can change the orientation manually:

1. Install [SQLite Browser](https://sqlitebrowser.org/dl/).
2. Plug your Kindle into your computer and
   open `.active_content_sandbox/browser/resource/LocalStorage/file__0.localstorage` in it.
3. Switch to the "Browse Data" tab.
4. Click on the `value` column in the `settings` row.
5. In the JSON editor on the right, change the `orientation` value from `auto` to one of:
    - `auto` (default)
    - `portrait`
    - `portraitUp`
    - `portraitDown`
    - `landscape`
    - `landscapeLeft` (top of the device on the left)
    - `landscapeRight` (top of the device on the right)
6. Click "Apply" under the JSON editor
7. Click "Write Changes" at the top.
8. Safely eject the Kindle from your PC. Next time you open the browser, it should be in the specified orientation.

_Instructions by [somethingelse on MobileRead Forums](https://www.mobileread.com/forums/showthread.php?p=4188284)_

## SkipStone

_Disclaimer: Jailbreaking is NOT necessary to use WeatherBoard. I am NOT responsible for any damage to your device,
whether it is a result of an attempted or successful jailbreak or not. This is all at your own risk._

If you want a smaller browser UI, you
can [jailbreak your Kindle](https://www.mobileread.com/forums/showthread.php?t=203326) and install
the [SkipStone](http://www.fabiszewski.net/kindle-browser/) browser. I have done this and it looks like this:

![running in SkipStone](doc/skipstone.png)

## Generating images instead

After a week of testing, I encountered problems where SkipStone would start failing to render the page fully
and decided to switch to rendering PNG files on the server and displaying them on the device using `eips`.

Using this method requires [wkhtmltopdf](https://wkhtmltopdf.org/) to be installed on the server.
Supply the executable path in the configuration file along with the size (and optionally orientation)
of the image to generate. Once this is configured, the image can be fetched from `http://{server IP}:8333/image`.

```toml
[Image]
WkHtmlToImage = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltoimage.exe" # Windows
# OR
WkHtmlToImage = "/usr/bin/wkhtmltoimage" # Linux

# Kindle Paperwhite
Width = 1024
Height = 758
# OR
# Kindle 4
Width = 800
Height = 600

# Optional, depends on physical device orientation
Rotate = "" # Keep device in portrait and use "CW", "CCW", or "flip" here
```

If this is not installed or configured, `/image` will not work, but the HTML webpage will still function as before.

### Displaying images on the Kindle

Jailbroken Kindle devices can fetch and display images on the screen using only shell scripts, as done by
[Dennis Reimann](https://github.com/dennisreimann/kindle-display).

1. Connect to a Kindle over Telnet or SSH using [USBNetwork](https://wiki.mobileread.com/wiki/USBNetwork0)
2. Place this script at `/mnt/us/weather.sh`:
   ```shell
   SERVER='http://{server IP}:8333'
   FILE='/tmp/weather.png'
   
   # Disable the built-in screensaver
   lipc-set-prop com.lab126.powerd preventScreenSaver 1
   
   rm "$FILE"
   eips -c # clear screen
   
   if wget "$SERVER/image" -O "$FILE"; then
     eips -f -g "$FILE"
   else
     eips -h 10 10 'Error getting image' 
   fi
   ```
   
3. Run `chmod +x /mnt/us/weather.sh` to allow executing the script.

4. Run `/mnt/us/weather.sh` to make sure everything works.
   The device's display should show the rendered image.

5. Add a line at the end of root's crontab at `/etc/crontab/root`:
   ```shell
   */15 * * * * /mnt/us/weather.sh
   ```
   This will refresh the weather image every 15 minutes.
