package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

type StateResponse struct {
	EntityID    string                 `json:"entity_id"`
	State       string                 `json:"state"`
	LastChanged time.Time              `json:"last_changed"`
	LastUpdated time.Time              `json:"last_updated"`
	Attributes  map[string]interface{} `json:"attributes"`
}

func (state *StateResponse) Attribute(attribute string) string {
	if value, exists := state.Attributes[attribute]; !exists {
		return ""
	} else if str, stringOk := value.(string); !stringOk {
		return ""
	} else {
		return str
	}
}

func ApiRequest(method string, endpoint string, body io.Reader) (responseData []byte, err error) {
	baseUrl := strings.TrimSuffix(config.HA.InstanceURL, "/")

	endpoint = strings.TrimPrefix(endpoint, "/")
	endpoint = strings.TrimPrefix(endpoint, "api")
	endpoint = strings.TrimPrefix(endpoint, "/")

	var request *http.Request
	if request, err = http.NewRequest(method, baseUrl+"/api/"+endpoint, body); err != nil {
		err = fmt.Errorf("error formatting API request: %w", err)
		return
	}

	request.Header.Set("Accept", "application/json")
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", "Bearer "+config.HA.Token)

	var response *http.Response
	if response, err = http.DefaultClient.Do(request); err != nil {
		err = fmt.Errorf("error making API request: %w", err)
		return
	}

	defer func() {
		if closeErr := response.Body.Close(); closeErr != nil {
			log.Printf("Error closing API response body: %v\n", closeErr)
		}
	}()

	if response.StatusCode >= 400 {
		err = fmt.Errorf("HTTP %s", response.Status)
		log.Printf("API error @ %s: HTTP %d (%s)\n", request.URL, response.StatusCode, response.Status)
		return
	}

	if responseData, err = io.ReadAll(response.Body); err != nil {
		err = fmt.Errorf("error reading API response: %w", err)
	}
	return
}

func GetState(entityID string) (state *StateResponse, err error) {
	var response []byte
	if response, err = ApiRequest("GET", "states/"+entityID, nil); err != nil {
		err = fmt.Errorf("error making API request: %w", err)
		return
	}

	state = new(StateResponse)
	if err = json.Unmarshal(response, state); err != nil {
		err = fmt.Errorf("error parsing weather response JSON: %w", err)
		return
	}

	return
}
