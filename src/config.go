package main

import (
	"github.com/BurntSushi/toml"
	"log"
	"os"
)

const configFileName = "weatherboard_config.toml"

type Config struct {
	HA      HaConfig      `toml:"HomeAssistant"`
	Weather WeatherConfig `toml:"Weather"`
	Web     WebConfig     `toml:"Web"`
	Image   ImageConfig   `toml:"Image"`
}

type HaConfig struct {
	InstanceURL string `toml:"URL"`
	Token       string `toml:"Token"`
}

type WeatherConfig struct {
	EntityID           string   `toml:"EntityID"`
	OtherStates        []string `toml:"OtherStates"`
	OtherStatesColumns uint     `toml:"OtherStatesColumns"`
}

func (weatherConfig *WeatherConfig) ActualOtherStatesColumns() uint {
	if weatherConfig.OtherStatesColumns > 0 {
		return weatherConfig.OtherStatesColumns
	}

	return 1
}

type WebConfig struct {
	ListenAddress string `toml:"ListenAddress"`
	Refresh       uint16 `toml:"RefreshSeconds"`
}

type ImageConfig struct {
	WkHtmlToImage string `toml:"WkHtmlToImage"`
	Width         int    `toml:"Width"`
	Height        int    `toml:"Height"`
	Rotate        string `toml:"Rotate"` // "CW", "CCW", or "flip"
}

func loadConfig() (config *Config, err error) {
	config = new(Config)
	config.Web.Refresh = 900
	config.Web.ListenAddress = ":8333"

	var configFile *os.File
	if configFile, err = os.Open(configFileName); err != nil {
		err = writeExampleConfig()
		return
	}

	_, err = toml.DecodeFile(configFileName, &config)

	if closeErr := configFile.Close(); closeErr != nil {
		log.Printf("Error closing config file: %v", closeErr)
	}

	return
}

func writeExampleConfig() (err error) {
	var configFile *os.File

	if configFile, err = os.Create(configFileName); err != nil {
		return
	}

	defer func() {
		if closeErr := configFile.Close(); closeErr != nil {
			log.Printf("Error closing config file: %v\n", closeErr)
		}
	}()

	err = toml.NewEncoder(configFile).Encode(Config{})
	return
}
