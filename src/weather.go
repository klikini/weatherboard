package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type WeatherStateResponse struct {
	StateResponse
	Attributes WeatherAttributes `json:"attributes"`
}

func (weather *WeatherStateResponse) ConditionName(condition string) string {
	if condition == "partlycloudy" {
		condition = "partly cloudy"
	} else {
		condition = strings.ReplaceAll(condition, "-", " ")
	}

	return strings.ToUpper(string(condition[0])) + condition[1:]
}

type WeatherAttributes struct {
	Temperature       float32 `json:"temperature"`
	TemperatureUnit   string  `json:"temperature_unit"`
	Humidity          float32 `json:"humidity"`
	Pressure          float32 `json:"pressure"`
	PressureUnit      string  `json:"pressure_unit"`
	WindBearing       float32 `json:"wind_bearing"`
	WindSpeed         float32 `json:"wind_speed"`
	WindSpeedUnit     string  `json:"wind_speed_unit"`
	VisibilityUnit    string  `json:"visibility_unit"`
	PrecipitationUnit string  `json:"precipitation_unit"`
	Attribution       string  `json:"attribution"`
	FriendlyName      string  `json:"friendly_name"`
}

type WeatherForecastEntry struct {
	Condition                string    `json:"condition"`
	PrecipitationProbability float32   `json:"precipitation_probability"`
	DateTime                 time.Time `json:"datetime"`
	WindBearing              float32   `json:"wind_bearing"`
	Temperature              float32   `json:"temperature"`
	LowTemperature           float32   `json:"templow"`
	Pressure                 float32   `json:"pressure"`
	WindSpeed                float32   `json:"wind_speed"`
	Precipitation            float32   `json:"precipitation"`
}

func (entry *WeatherForecastEntry) Date() string {
	return string(entry.DateTime.Format("Monday")[0]) + " " + entry.DateTime.Format("1/2")
}

func (entry *WeatherForecastEntry) IsToday() bool {
	dY, dM, dD := entry.DateTime.In(time.Local).Date()
	tY, tM, tD := time.Now().In(time.Local).Date()
	return dY == tY && dM == tM && dD == tD
}

func GetWeather() (weather *WeatherStateResponse, err error) {
	var response []byte
	if response, err = ApiRequest("GET", "states/"+config.Weather.EntityID, nil); err != nil {
		err = fmt.Errorf("error making API request: %w", err)
		return
	}

	weather = new(WeatherStateResponse)
	if err = json.Unmarshal(response, weather); err != nil {
		err = fmt.Errorf("error parsing weather response JSON: %w", err)
		return
	}

	return
}

func GetForecast() (forecast []WeatherForecastEntry, err error) {
	requestData, _ := json.Marshal(map[string]string{
		"entity_id": config.Weather.EntityID,
		"type":      "daily",
	})

	var resultMsg json.RawMessage

	if resultMsg, err = ApiRequest(
		"POST", "services/weather/get_forecasts?return_response",
		bytes.NewBuffer(requestData),
	); err != nil {
		err = fmt.Errorf("error getting weather forecast: %w", err)
		return
	}

	result := struct {
		ServiceResponse map[string]struct {
			Forecast []WeatherForecastEntry `json:"forecast"`
		} `json:"service_response"`
	}{}

	if err = json.Unmarshal(resultMsg, &result); err != nil {
		err = fmt.Errorf("failed to parse weather forecast JSON: %w", err)
		return
	}

	forecast = result.ServiceResponse[config.Weather.EntityID].Forecast
	return
}
