package main

import (
	"bytes"
	"embed"
	"fmt"
	"github.com/ninetwentyfour/go-wkhtmltoimage"
	"html/template"
	"image"
	"image/draw"
	"image/png"
	"log"
	"math"
	"net/http"
	"strings"
)

//go:embed static
var staticFS embed.FS

//go:embed templates
var templateFS embed.FS

var templates *template.Template

type otherState struct {
	Name  string
	Value string
	Unit  string
}

type dashboardContext struct {
	RefreshInterval uint16
	WeatherData     *WeatherStateResponse
	WeatherForecast []WeatherForecastEntry
	HighToday       int16
	OtherStates     [][]otherState // row-major
}

func WebTask() {
	var err error

	if templates, err = template.ParseFS(templateFS, "templates/*.gohtml"); err != nil {
		log.Fatalf("Failed to parse templates: %v\n", err)
	}

	mux := http.NewServeMux()
	mux.Handle("/static/", http.FileServer(http.FS(staticFS)))
	mux.HandleFunc("/", dashboardPage)
	mux.HandleFunc("/image", dashboardImage)

	log.Printf("Web server listening on %s\n", config.Web.ListenAddress)
	if err = http.ListenAndServe(config.Web.ListenAddress, mux); err != nil {
		log.Fatalf("HTTP server failed: %v\n", err)
	}
}

func dashboardPage(response http.ResponseWriter, _ *http.Request) {
	var err error

	data := dashboardContext{
		RefreshInterval: config.Web.Refresh,
	}

	if config.Weather.EntityID != "" {
		if data.WeatherData, err = GetWeather(); err != nil {
			response.WriteHeader(500)
			log.Printf("Error getting weather: %v", err)
			return
		}

		if data.WeatherForecast, err = GetForecast(); err != nil {
			response.WriteHeader(500)
			log.Printf("Error getting forecast: %v", err)
			return
		}
	}

	if len(data.WeatherForecast) > 0 && data.WeatherForecast[0].IsToday() {
		data.HighToday = int16(math.Round(float64(data.WeatherForecast[0].Temperature)))
	}

	osColumnCount := config.Weather.ActualOtherStatesColumns()
	var osRow []otherState
	osColumn := uint(0)

	for _, entityId := range config.Weather.OtherStates {
		var state *StateResponse
		if state, err = GetState(entityId); err != nil {
			log.Printf("Error getting '%s': %v", entityId, err)
		} else {
			osRow = append(osRow, otherState{
				Name:  state.Attribute("friendly_name"),
				Value: state.State,
				Unit:  state.Attribute("unit_of_measurement"),
			})

			osColumn++

			if osColumn == osColumnCount {
				data.OtherStates = append(data.OtherStates, osRow)
				osRow = []otherState{}
				osColumn = 0
			}
		}
	}

	data.OtherStates = append(data.OtherStates, osRow)

	if err = templates.ExecuteTemplate(response, "index.gohtml", &data); err != nil {
		response.WriteHeader(500)
		log.Printf("Failed to render template: %v\n", err)
	}
}

func dashboardImage(response http.ResponseWriter, _ *http.Request) {
	inputUrl := "http://"
	if strings.HasPrefix(config.Web.ListenAddress, ":") {
		// :port
		inputUrl += "127.0.0.1" + config.Web.ListenAddress
	} else if strings.Contains(config.Web.ListenAddress, ":") {
		// ip:port
		inputUrl += config.Web.ListenAddress
	} else {
		// default
		inputUrl += "127.0.0.1:8333"
	}

	options := wkhtmltoimage.ImageOptions{
		BinaryPath: config.Image.WkHtmlToImage,
		Input:      inputUrl,
		Format:     "png",
		Width:      config.Image.Width,
		Height:     config.Image.Height,
		Quality:    100, // %
	}

	var err error
	var imageBuf []byte

	if imageBuf, err = wkhtmltoimage.GenerateImage(&options); err != nil {
		log.Printf("Error generating image: %v\n", err)
		return
	}

	if config.Image.Rotate != "" {
		if imageBuf, err = rotateImage(imageBuf, config.Image.Rotate); err != nil {
			log.Printf("Error rotating image: %v\n", err)
			return
		}
	}

	response.Header().Set("Content-Type", "image/png")
	response.WriteHeader(200)
	_, _ = response.Write(imageBuf)
}

func rotateImage(srcBuf []byte, orientation string) (dstBuf []byte, err error) {
	var srcImg image.Image
	var dstImg draw.Image

	if srcImg, _, err = image.Decode(bytes.NewReader(srcBuf)); err != nil {
		log.Printf("Error decoding image: %v\n", err)
		return
	}

	srcWidth := srcImg.Bounds().Dx()
	srcHeight := srcImg.Bounds().Dy()

	switch orientation {
	case "CW":
		dstImg = image.NewGray(image.Rect(0, 0, srcHeight, srcWidth))

		for x := 0; x < srcWidth; x++ {
			for y := 0; y < srcHeight; y++ {
				dstImg.Set(y, x, srcImg.At(x, srcHeight-y))
			}
		}
	case "CCW":
		dstImg = image.NewGray(image.Rect(0, 0, srcHeight, srcWidth))

		for x := 0; x < srcWidth; x++ {
			for y := 0; y < srcHeight; y++ {
				dstImg.Set(y, x, srcImg.At(srcWidth-x, y))
			}
		}
	case "flip":
		dstImg = image.NewGray(srcImg.Bounds())

		for x := 0; x < srcWidth; x++ {
			for y := 0; y < srcHeight; y++ {
				dstImg.Set(srcWidth-x, srcHeight-y, srcImg.At(x, y))
			}
		}
	default:
		err = fmt.Errorf("invalid orientation '%s', expected 'CW', 'CCW', or 'flip'", orientation)
		return
	}

	exportBuf := new(bytes.Buffer)
	err = png.Encode(exportBuf, dstImg)
	dstBuf = exportBuf.Bytes()
	return
}
