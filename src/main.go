package main

import "log"

var config *Config

func main() {
	var err error

	if config, err = loadConfig(); err != nil {
		log.Fatalf("Error loading config: %v\n", err)
	}

	go WebTask()
	select {}
}
