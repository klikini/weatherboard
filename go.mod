module weatherboard

go 1.20

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/ninetwentyfour/go-wkhtmltoimage v0.0.0-20150201222019-3ccfacb98ac2
)
